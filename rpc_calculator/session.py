# code from https://lucumr.pocoo.org/2015/4/8/microservices-with-nameko/

from weakref import WeakKeyDictionary

from nameko.extensions import DependencyProvider
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class Session(DependencyProvider):

    def __init__(self, declarative_base):
        self.declarative_base = declarative_base
        self.sessions = WeakKeyDictionary()

    def get_dependency(self, worker_ctx):
        db_uri = self.container.config['DATABASE_URL']
        engine = create_engine(db_uri)
        self.declarative_base.metadata.create_all(engine)
        session_cls = sessionmaker(bind=engine)
        self.sessions[worker_ctx] = session = session_cls()
        return session

    def worker_teardown(self, worker_ctx):
        session = self.sessions.pop(worker_ctx, None)
        if session is not None:
            session.close()
