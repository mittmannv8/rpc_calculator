from sqlalchemy import Column, Float, String
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy_utils.types.choice import ChoiceType
from sqlalchemy_utils.types.uuid import UUIDType


Base = declarative_base()


class Result(Base):
    __tablename__ = 'result'

    STATUS = [
        ('succeeded', 'Succeeded'),
        ('failed', 'Failed')
    ]

    OPERATIONS = [
        ('sum', 'sum'),
        ('subtract', 'subtract'),
        ('multiply', 'multiply'),
        ('divide', 'divide')
    ]

    # id = Column(Integer, primary_key=True)
    uuid = Column(UUIDType(binary=False), primary_key=True)
    operation = Column(ChoiceType(OPERATIONS))
    status = Column(ChoiceType(STATUS))
    value = Column(Float, nullable=True)
    error = Column(String, nullable=True)
    worker = Column(String, nullable=True)

    def __repr__(self):
        return '<Result: {operation} {status} - {result}'.format(
            operation=self.operation,
            status=self.status,
            result=self.error or self.value
        )
