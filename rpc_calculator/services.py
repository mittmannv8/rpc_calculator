import os
from nameko.rpc import rpc
from typing import Callable, List
from uuid import uuid4

from .calculator import addition, division, multiplication, subtraction
from .database import Result, Base
from .session import Session


def persist(func: Callable):

    def wrapper(instance, *args, **kwargs):
        uuid = uuid4()
        print('Received request ({uuid}) {func}: args: {args}, kwargs: {kwargs}'.format(
            uuid=uuid,
            func=func.__name__,
            args=args,
            kwargs=kwargs
        ))
        _exception = None
        result = Result(
            uuid=uuid,
            operation=func.__name__,
            worker='{} ({})'.format(os.uname().nodename, os.getpid())
        )

        try:
            func_result = func(instance, *args, **kwargs)
            result.value = func_result
            result.status = 'succeeded'
        except Exception as expt:
            result.error = repr(expt)
            result.status = 'failed'
            _exception = expt
        instance.session.add(result)
        instance.session.commit()

        print('Result of request ({uuid}): {result}'.format(
            uuid=uuid,
            result=result
        ))

        if _exception:
            raise _exception

        return func_result

    return wrapper


class CalculatorService:
    name = 'calculator'

    session = Session(Base)

    @rpc
    @persist
    def sum(self, numbers: List[int]):
        return addition(numbers)

    @rpc
    @persist
    def subtract(self, numbers):
        return subtraction(numbers)

    @rpc
    @persist
    def multiply(self, numbers):
        return multiplication(numbers)

    @rpc
    @persist
    def divide(self, numbers):
        return division(numbers)
