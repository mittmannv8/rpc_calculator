import operator
from decimal import Decimal
from functools import reduce
from typing import List

from .exceptions import InvalidPayload


def validate_payload(payload: List = None):
    if not payload or not isinstance(payload, (list, tuple)):
        raise InvalidPayload()

    for i in payload:
        if not isinstance(i, (int, float, Decimal)):
            raise InvalidPayload()


def addition(numbers: List[int]) -> int:
    validate_payload(numbers)
    return sum(numbers)


def subtraction(numbers: List) -> int:
    validate_payload(numbers)
    return reduce(operator.sub, numbers)


def multiplication(numbers: List) -> int:
    validate_payload(numbers)
    return reduce(operator.mul, numbers)


def division(numbers: List) -> float:
    validate_payload(numbers)
    return reduce(operator.truediv, numbers)
