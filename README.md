# RPC Calculator

### Instalation
You can install the app through pip (I strongly recommend use virtualenv our venv module):

```
pip install -r requirements.txt
```

### Dependencies (RabbitMQ)
This project uses RabbitMQ as message broker. You can install it, use a remote server or just use Docker.
If you are using Docker, just run:
```
make start_server
```


### Settings
You should define the settings in file `config.yaml`

```
AMQP_URI - URI to RabbitMQ
DATABASE_URL - URI to SQLite database file
```

### Running
Before run the clients, ensure that the RabbitMQ is running.

To start a client:
```
make start_client
```

You can start more than one client in another terminal.


### Samples
If you just want to run some samples, you can execute (at least one client instance must be running):
```
make request_samples
```

If you are using virtual environment, don't forget to load the environment because this runner needs the project dependencies.


### Tests
This project was tested using pytest. If you are using a virtual environment, please install development requirements
```
pip install -r requirements-dev.txt
```

To run the tests, you can:
```
make test
```