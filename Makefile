start_server:
	docker run -d -p 5672:5672 rabbitmq:3

start_client:
	nameko run rpc_calculator.services --config ./config.yaml

test:
	pytest -s --disable-pytest-warnings

request_samples:
	python run_rpc_samples.py
