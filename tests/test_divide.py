import pytest

from rpc_calculator import calculator
from rpc_calculator.exceptions import InvalidPayload


def test_divide_samples():
    assert calculator.division([1, 2, 3]) == 0.16666666666666666
    assert calculator.division([3, 2, 1]) == 1.5
    assert calculator.division([9, 3]) == 3.0
    assert calculator.division([27, 9, 3]) == 1.0
    assert calculator.division([2, 3]) == 0.6666666666666666
    assert calculator.division([0, 9]) == 0.0


def test_divide_with_negative_numbers():

    result = calculator.division([1, -2])
    assert result == -0.5


def test_divide_by_zero():

    with pytest.raises(ZeroDivisionError):
        calculator.division([10, 9, 0])


def test_divide_empty_list():

    with pytest.raises(InvalidPayload):
        calculator.division([])


def test_divide_with_no_payload():

    with pytest.raises(TypeError):
        calculator.division()


def test_divide_with_invalid_payload():

    with pytest.raises(InvalidPayload):
        calculator.division(['1', 2])
