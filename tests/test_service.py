import pytest
from nameko.testing.services import worker_factory

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from rpc_calculator.services import CalculatorService
from rpc_calculator.exceptions import InvalidPayload
from rpc_calculator.database import Base, Result


@pytest.fixture
def session():
    engine = create_engine('sqlite:///:memory:')
    Base.metadata.create_all(engine)
    session_cls = sessionmaker(bind=engine)
    return session_cls()


def test_sum_service(session):
    service = worker_factory(CalculatorService, session=session)

    service.sum([1, 2, 3])
    assert session.query(Result.value).count() == 1

    result = session.query(Result).one()
    assert result.value == 6
    assert result.operation == 'sum'
    assert result.status == 'succeeded'


def test_sum_service_two_workers(session):
    service1 = worker_factory(CalculatorService, session=session)
    service2 = worker_factory(CalculatorService, session=session)

    service1.sum([1, 2, 3])
    service2.sum([1, 2, 3])
    assert session.query(Result).count() == 2


def test_sum_service_with_invalid_payload(session):
    service = worker_factory(CalculatorService, session=session)
    with pytest.raises(InvalidPayload):
        service.sum(['a', 'b'])

    assert session.query(Result).count() == 1
    result = session.query(Result).one()
    assert result.status == 'failed'
    assert result.error == 'InvalidPayload()'


def test_subtract_service(session):
    service = worker_factory(CalculatorService, session=session)

    service.subtract([1, 2, 3])
    assert session.query(Result.value).all() == [(-4,)]


def test_multiply_service(session):
    service = worker_factory(CalculatorService, session=session)

    service.multiply([1, 2, 3])
    service.multiply([0])
    assert session.query(Result.value).all() == [(6,), (0,)]


def test_divide_service(session):
    service = worker_factory(CalculatorService, session=session)

    service.divide([6, 2])
    assert session.query(Result.value).all() == [(3,)]


def test_divide_service_division_by_zero_error(session):
    service = worker_factory(CalculatorService, session=session)
    with pytest.raises(ZeroDivisionError):
        service.divide([2, 0])

    result = session.query(Result).one()
    assert result.status == 'failed'
    assert result.operation == 'divide'
    assert result.error == "ZeroDivisionError('division by zero')"

