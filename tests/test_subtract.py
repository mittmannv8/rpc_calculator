import pytest

from rpc_calculator import calculator
from rpc_calculator.exceptions import InvalidPayload


def test_subract_samples():

    assert calculator.subtraction([1, 2, 3]) == -4
    assert calculator.subtraction([3, 2, 1]) == 0
    assert calculator.subtraction([10, -2, 0]) == 12
    assert calculator.subtraction([-1, 2, -63]) == 60


def test_subtract_empty_list():

    with pytest.raises(InvalidPayload):
        calculator.subtraction([])


def test_subtract_with_no_payload():

    with pytest.raises(TypeError):
        calculator.subtraction()


def test_sum_with_invalid_payload():

    with pytest.raises(InvalidPayload):
        calculator.subtraction(['1', 2])
