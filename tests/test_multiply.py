import pytest

from rpc_calculator import calculator
from rpc_calculator.exceptions import InvalidPayload


def test_multiply_samples():
    assert calculator.multiplication([3, 3, 3]) == 27
    assert calculator.multiplication([0, 2, 5]) == 0
    assert calculator.multiplication([1, 1, 1]) == 1
    assert calculator.multiplication([7, -3, 0]) == 0
    assert calculator.multiplication([3, 3, 3, 6, 90]) == 14580
    assert calculator.multiplication([-1, -3]) == 3
    assert calculator.multiplication([-3, -3, -3]) == -27
    assert calculator.multiplication([0]) == 0


def test_multiply_empty_list():

    with pytest.raises(InvalidPayload):
        calculator.multiplication([])


def test_multiply_with_no_payload():

    with pytest.raises(TypeError):
        calculator.multiplication()


def test_multiply_with_invalid_payload():

    with pytest.raises(InvalidPayload):
        calculator.multiplication(['1', 2])


def test_multiply_with_negative_numbers():

    result = calculator.multiplication([1, -2])
    assert result == -2
