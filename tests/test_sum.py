import pytest

from rpc_calculator import calculator
from rpc_calculator.exceptions import InvalidPayload


def test_sum():
    assert calculator.addition([1, 2, 3]) == 6


def test_sum_empty_list():
    with pytest.raises(InvalidPayload):
        calculator.addition([])


def test_sum_with_no_payload():

    with pytest.raises(TypeError):
        calculator.addition()


def test_sum_with_invalid_payload():

    with pytest.raises(InvalidPayload):
        calculator.addition(['1', 2])


def test_sum_with_negative_numbers():

    assert calculator.addition([1, -2]) == -1
