from nameko.standalone.rpc import ClusterRpcProxy

config = {
    'AMQP_URI': 'pyamqp://guest:guest@localhost'
}


with ClusterRpcProxy(config) as cluster_rpc:
    operations = (
        ('sum', [1, 2]),
        ('sum', [0, 2, 9, 10]),
        ('subtract', [1, 1]),
        ('subtract', [-1, -2]),
        ('multiply', [3, 3]),
        ('multiply', [-1, 10]),
        ('divide', [10, 2]),
        ('divide', [4, 0]),
    )

    for operation, payload in operations:
        try:
            print(getattr(cluster_rpc.calculator, operation)(payload))
        except Exception as expt:
            print(expt)
